const config = {
    type: Phaser.AUTO,
    parent: 'game',
    width: 800,
    height: 600,
    backgroundColor: '#304858',
    scene: {
        preload,
        create,
        update
    }
};

const game = new Phaser.Game(config);


function preload () {
    this.load.atlas('walker', 'assets/walker.png', 'assets/walker.json');
    this.load.image('sky', 'assets/ms3-sky.png');
    this.load.image('trees', 'assets/ms3-trees.png');
}
function create () {
    this.bg = this.add.tileSprite(0, 38, 800, 296, 'sky').setOrigin(0, 0);
    this.trees = this.add.tileSprite(0, 280, 800, 320, 'trees').setOrigin(0, 0);

    const animConfig = {
        key: 'walk',
        frames: 'walker',
        frameRate: 40,
        repeat: -1 
    };
    this.anims.create(animConfig);

    const sprite = this.add.sprite(400, 484, 'walker');
    sprite.play('walk');
      // multiple
    // const sprites = [ this.sprite1, this.sprite2, this.sprite3, this.sprite4 ];
    // sprites.forEach( (sprite, index) => {
    //     let x = (index * 200) + 100;
    //     let y = 484;
    //     sprite = this.add.sprite(x, y, 'walker', 'frame_0001');
    //     sprite.setScale(0.5) 
    //     sprite.play('walk'); 
    // });

}
function update (){
    this.bg.tilePositionX -= 6;
    this.trees.tilePositionX -= 6;
}
 